import java.util.*;
import java.lang.*;
import java.text.*;

public class FuzzyKMeans
{
	double atr1[] = {1, 2, 1, 2, 1};
	double atr2[] = {1, 3, 2, 1, 3};
	double dataSet[][] = 
		{
			atr1, atr2
		};
	
	double k1[] = {0.7, 0.3, 0.4, 0.5, 0.6};
	double k2[] = {0.3, 0.7, 0.6, 0.5, 0.4};
	double k1_W[] = {Math.pow(k1[0], 2), Math.pow(k1[1], 2), Math.pow(k1[2], 2), Math.pow(k1[3], 2), Math.pow(k1[4], 2)};
	double k2_W[] = {Math.pow(k2[0], 2), Math.pow(k2[1], 2), Math.pow(k2[2], 2), Math.pow(k2[3], 2), Math.pow(k2[4], 2)};
	double matrixUIK[][] = 
		{
			k1, k2, k1_W, k2_W
		};
	double sigmaK1_W = 0;
	double sigmaK2_W = 0;
	
	public double clusterC1[][] = new double[dataSet.length][matrixUIK[0].length];
	double sigmaC1_1 = 0;
	double sigmaC1_2 = 0;
	
	public double clusterC2[][] = new double[dataSet.length][matrixUIK[0].length];
	double sigmaC2_1 = 0;
	double sigmaC2_2 = 0;
	
	public double cluster[][] = new double[dataSet.length][dataSet.length];
	
	public FuzzyKMeans()
	{
		setSigmaK1_W();
		setSigmaK2_W();
		setClusterC1();
		setClusterC2();
		setCluster();
	}
	
	public void setDataSet()
	{
		
	}
	
	public void setClusterC1()
	{
		for(int i=0; i<clusterC1[0].length; i++)
		{
			clusterC1[0][i] = matrixUIK[2][i]*dataSet[0][i];
			clusterC1[1][i] = matrixUIK[3][i]*dataSet[0][i];
		}
		
		setSigmaC1();
		
	}
	
	public void setSigmaC1()
	{
		for(int i=0; i<clusterC1[0].length; i++)
		{
			this.sigmaC1_1 += clusterC1[0][i];
		}
		
		for(int i=0; i<clusterC1[0].length; i++)
		{
			this.sigmaC1_2 += clusterC1[1][i];
		}
	}

	public void setClusterC2()
	{
		for(int i=0; i<clusterC2[0].length; i++)
		{
			clusterC2[0][i] = matrixUIK[2][i]*dataSet[1][i];
			clusterC2[1][i] = matrixUIK[3][i]*dataSet[1][i];
		}
		
		setSigmaC2();
		
	}	
	
	public void setSigmaC2()
	{
		for(int i=0; i<clusterC2[0].length; i++)
		{
			this.sigmaC2_1 += clusterC2[0][i];
		}
		
		for(int i=0; i<clusterC1[0].length; i++)
		{
			this.sigmaC2_2 += clusterC2[1][i];
		}
	}
	
	public void setCluster()
	{
		this.cluster[0][0] = sigmaC1_1/sigmaK1_W;
		this.cluster[0][1] = sigmaC1_2/sigmaK1_W;
		this.cluster[1][0] = sigmaC2_1/sigmaK2_W;
		this.cluster[1][1] = sigmaC2_2/sigmaK2_W;
	}
	
	public void setSigmaK1_W()
	{
		for(int i=0; i<matrixUIK[0].length; i++)
		{
			this.sigmaK1_W += matrixUIK[2][i];
		}
	}

	public void setSigmaK2_W()
	{
		for(int i=0; i<matrixUIK[0].length; i++)
		{
			this.sigmaK2_W += matrixUIK[3][i];
		}
	}
	
	public double getSigmaK1_W()
	{
		return sigmaK1_W;
	}
	
	public double getSigmaK2_W()
	{
		return sigmaK2_W;
	}
	
	public double getSigmaC1_1()
	{
		return sigmaC1_1;
	}
	
	public double getSigmaC1_2()
	{
		return sigmaC1_2;
	}
	
	public double getSigmaC2_1()
	{
		return sigmaC2_1;
	}
	
	public double getSigmaC2_2()
	{
		return sigmaC2_2;
	}
	
	public void printDataSet()
	{
		System.out.println("\nData Set");
		System.out.println("No\tAtribut1\tAtribut2");
		for(int i=0; i<dataSet[0].length; i++)
		{
			System.out.println((i+1)+"\t"+dataSet[0][i]+"\t\t"+dataSet[1][i]);
		}
	}
	
	public void printMatrikUIK()
	{	
		System.out.println("\nMatrix UIK");
		System.out.println("No\tk1\tk2\tkl^w\tk2^w");
		for(int i=0; i<matrixUIK[0].length; i++)
		{
			System.out.println((i+1)+"\t"+Double.parseDouble(new DecimalFormat("##.##").format(matrixUIK[0][i]))+
			"\t"+Double.parseDouble(new DecimalFormat("##.##").format(matrixUIK[1][i]))+
			"\t"+Double.parseDouble(new DecimalFormat("##.##").format(matrixUIK[2][i]))+
			"\t"+Double.parseDouble(new DecimalFormat("##.##").format(matrixUIK[3][i])));
		}
		System.out.println("Sigma k1^w = "+getSigmaK1_W());
		System.out.println("Sigma k2^w = "+getSigmaK2_W());
	}
	
	public void printCenterCluster()
	{	
		System.out.println("\nPusat Cluster");
		System.out.println("No\tk1^2*A1\tk2^2*A1");
		for(int i=0; i<clusterC1[0].length; i++)
		{
			System.out.println((i+1)+"\t"+Double.parseDouble(new DecimalFormat("##.##").format(clusterC1[0][i]))+
			"\t"+Double.parseDouble(new DecimalFormat("##.##").format(clusterC1[1][i]))
			);
		}
		System.out.println("Sigma k1^w = "+getSigmaC1_1());
		System.out.println("Sigma k2^w = "+getSigmaC1_2());
		
		System.out.println("No\tk1^2*A2\tk2^2*A2");
		for(int i=0; i<clusterC2[0].length; i++)
		{
			System.out.println((i+1)+"\t"+Double.parseDouble(new DecimalFormat("##.##").format(clusterC2[0][i]))+
			"\t"+Double.parseDouble(new DecimalFormat("##.##").format(clusterC2[1][i]))
			);
		}
		System.out.println("Sigma k1^w = "+getSigmaC2_1());
		System.out.println("Sigma k2^w = "+getSigmaC2_2());
	}
	
	public void printCluster()
	{
		System.out.println("\nCluster");
		System.out.println("Class\tAtribut1\tAtribut2");
		for(int i=0; i<cluster[0].length; i++)
		{
			System.out.println((i+1)+"\t"+Double.parseDouble(new DecimalFormat("##.######").format(cluster[0][i]))+
			"\t"+Double.parseDouble(new DecimalFormat("##.######").format(cluster[1][i])));
		}
	}
	
	public static void main(String args[])
	{
		FuzzyKMeans FK = new FuzzyKMeans();
		FK.printDataSet();
		FK.printMatrikUIK();
		FK.printCenterCluster();
		FK.printCluster();
	}
	
}